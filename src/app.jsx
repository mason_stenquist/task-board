import Board from "./components/Board";
import Template from "./template/Template";
import { Filters } from "./components/filters/Filters";

export function App(props) {
  return (
    <Template>
      <Filters />
      <Board />
    </Template>
  );
}
