import { fmScript } from "fmcperformscript";
import { fmFind } from "./fmFind.js";

export const loadData = async () => {
  const tasksQuery = {
    limit: 200,
    query: [
      {
        z__WeekNo_Schedule: "2021.43",
      },
    ],
  };

  const teammatesQuery = {
    limit: 50,
    query: [
      {
        Current: "1",
      },
    ],
  };

  try {
    // const [taskResults, teammateResults] = await fmScript("fm-bridge-query", [
    //   tasksQuery,
    //   teammatesQuery,
    // ]);

    const [taskResults, teammateResults] = await Promise.all([
      fmFind("Task-JSON", tasksQuery, true, 60),
      fmFind("Teammate-JSON", teammatesQuery, true, 60),
    ]);

    //Process Tasks
    const tasks = taskResults.response.data.map((record) =>
      JSON.parse(record.fieldData.zctTaskJSON)
    );

    //Process Teammates
    const teammates = teammateResults.response.data.map((record) => ({
      ...JSON.parse(record.fieldData.zctTeammateJSON),
      photo: record.fieldData.Photo,
    }));
    return [tasks, teammates];
  } catch (e) {
    console.log(e);
    alert("Error getting teammate data");
  }
};

export const getTasks = async () => {
  const query = {
    layouts: "Task-JSON",
    limit: 50,
    query: [
      {
        z__WeekNo_Schedule: "2021.40",
        z__TeammateUUID: "*",
      },
    ],
    sort: [{ fieldName: "Priority_SortNum", sortOrder: "ascend" }],
  };

  try {
    const results = await fmScript("fm-bridge-query", query);
    const records = results.response.data.map((record) =>
      JSON.parse(record.fieldData.zctTaskJSON)
    );
    return records;
  } catch (e) {
    console.log(e);
  }
};

export const getTeammates = async () => {
  const query = {
    layouts: "Teammate-JSON",
    limit: 50,
    query: [
      {
        Current: "1",
      },
    ],
  };

  try {
    const results = await fmScript("fm-bridge-query", query);
    const records = results.response.data.map((record) => ({
      ...JSON.parse(record.fieldData.zctTeammateJSON),
      photo: record.fieldData.Photo,
    }));
    return records;
  } catch (e) {
    console.log(e);
    alert("Error getting teammate data");
  }
};
