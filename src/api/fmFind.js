export const fmFind = async (
  layout,
  post,
  useCache = true,
  cacheTime = 300
) => {
  return await (
    await fetch(`${import.meta.env.VITE_PROXY_URL}/layouts/${layout}/_find`, {
      method: "POST",
      headers: {
        username: import.meta.env.VITE_USERNAME,
        password: import.meta.env.VITE_PASSWORD,
        usecache: useCache,
        cachetime: cacheTime,
      },
      body: JSON.stringify(post),
    })
  ).json();
};
