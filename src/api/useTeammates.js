import { useQuery } from "react-query";
import { fmFind } from "./fmFind";

const USE_CACHE = false;
const CACHE_TIME = 60 * 60;

const unnassignedRow = {
  id: "nobody",
  nickName: "Nobody",
  collapsed: false,
  defaultCapacity: {
    monday: 0,
    tuesday: 0,
    wednesday: 0,
    thursday: 0,
    friday: 0,
  },
};

const useTeammates = (options) => {
  const getTeammates = async () => {
    const teammatesQuery = {
      limit: 200,
      query: [
        {
          Current: "1",
        },
      ],
      sort: [
        {
          fieldName: "Name_Nickname",
          sortOrder: "ascend",
        },
      ],
    };

    const teammateResults = await fmFind(
      "Teammate-JSON",
      teammatesQuery,
      USE_CACHE,
      CACHE_TIME
    );

    const teammates = teammateResults.response.data.map((record) => ({
      ...JSON.parse(record.fieldData.zctTeammateJSON),
      photo: record.fieldData.Photo,
      collapsed: true,
    }));
    return [unnassignedRow, ...teammates];
  };

  return useQuery("teammates", getTeammates, {
    keepPreviousData: true,
    ...options,
  });
};

export default useTeammates;
