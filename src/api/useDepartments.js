import { useQuery } from "react-query";

import { fmFind } from "./fmFind";

const USE_CACHE = true;
const CACHE_TIME = 60 * 60 * 24 * 7 * 10;

const useDepartments = (options) => {
  const getDepartments = async () => {
    const departmentsQuery = {
      limit: 500,
      query: [
        {
          Department: "*",
        },
      ],
    };

    const departmentResults = await fmFind(
      "Department-JSON",
      departmentsQuery,
      USE_CACHE,
      CACHE_TIME
    );
    const departments = departmentResults.response.data.map((record) => ({
      id: record.fieldData.z___UUID,
      name: record.fieldData.Department,
    }));
    return departments;
  };

  return useQuery(["departments"], getDepartments, {
    keepPreviousData: true,
    ...options,
  });
};

export default useDepartments;
