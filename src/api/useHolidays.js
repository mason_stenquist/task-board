import dayjs from "dayjs";
import { useQuery } from "react-query";
import { WEEKS_BEFORE } from "../components/filters/WeekFilter";
import { fmFind } from "./fmFind";

const USE_CACHE = true;
const CACHE_TIME = 60 * 60 * 24 * 5;

const useHolidays = (options) => {
  const getHolidays = async () => {
    const twoWeeksAgo = dayjs()
      .subtract(WEEKS_BEFORE, "week")
      .format("M/D/YYYY");
    const holidaysQuery = {
      limit: 200,
      query: [
        {
          date: `>= ${twoWeeksAgo}`,
        },
      ],
    };

    const holidayResults = await fmFind(
      "ScheduleEvent-JSON",
      holidaysQuery,
      USE_CACHE,
      CACHE_TIME
    );

    const holidays = holidayResults.response.data.map((record) => ({
      ...JSON.parse(record.fieldData.zctEventJSON),
    }));
    return holidays;
  };

  return useQuery("holidays", getHolidays, {
    keepPreviousData: true,
    ...options,
  });
};

export default useHolidays;
