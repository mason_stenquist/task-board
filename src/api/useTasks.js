import { useQuery } from "react-query";
import { useRecoilValue } from "recoil";
import { weekFilterAtom } from "../state";
import { fmFind } from "./fmFind";

const USE_CACHE = false;

const useTasks = (options) => {
  const weekFilter = useRecoilValue(weekFilterAtom);

  const getTasksByWeekNo = async () => {
    const tasksQuery = {
      limit: 500,
      query: [
        {
          z__WeekNo_Schedule: weekFilter,
          "task_project_RECORD::Type": "Order",
        },
      ],
      sort: [
        {
          fieldName: "Priority_SortNum",
          sortOrder: "ascend",
        },
        {
          fieldName: "z_sort",
          sortOrder: "ascend",
        },
      ],
    };

    const taskResults = await fmFind("Task-JSON", tasksQuery, USE_CACHE);
    const tasks = taskResults.response.data.map((record) =>
      JSON.parse(record.fieldData.zctTaskJSON)
    );
    return tasks;
  };

  return useQuery(["tasks", weekFilter], getTasksByWeekNo, {
    keepPreviousData: true,
    ...options,
  });
};

export default useTasks;
