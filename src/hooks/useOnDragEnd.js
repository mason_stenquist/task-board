import { fmScript } from "fmcperformscript";
import { UPDATE_TASK_SCRIPT } from "../constants";
import { useQueryClient } from "react-query";
import useTasks from "../api/useTasks";
import { useRecoilValue, useSetRecoilState } from "recoil";
import {
  draggingTaskAtom,
  scrollToAtom,
  testModeAtom,
  weekFilterAtom,
} from "../state";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
dayjs.extend(customParseFormat);

const unhideAllRows = () => {
  const rows = window.document.querySelectorAll(".row");
  for (let i = 0; i < rows.length; i++) {
    const row = rows[i];
    row.classList.remove("hidden");
  }
};

const updateProjectTasks = (
  projectTasks,
  destTeammateId,
  destDate,
  draggedTaskId
) => {
  const startDate = dayjs(destDate, "M/D/YYYY");
  return projectTasks.map((task, i) => {
    const newTask = {
      ...task,
      //date: startDate.add(i, "day").format("M/D/YYYY"),
    };

    //Only update the date if there the change is less than a week difference or there was no date.
    const newDate = startDate.add(i, "day");
    const projectTaskDate = dayjs(task.date, "M/D/YYYY");
    const difference = newDate.diff(projectTaskDate, "day");
    if (!destDate || !task.date || Math.abs(difference) < 7) {
      newTask.date = !destDate ? "" : newDate.format("M/D/YYYY");
    }

    //Only update the currently dragged tasks teammateID
    if (draggedTaskId === task.id) {
      newTask.teammateID = destTeammateId === "nobody" ? "" : destTeammateId;
    }
    return newTask;
  });
};

export const useOnDragEnd = (rows, setForceUpdate) => {
  const queryClient = useQueryClient();
  const taskQuery = useTasks();
  const weekFilter = useRecoilValue(weekFilterAtom);
  const testMode = useRecoilValue(testModeAtom);
  const setDraggingTask = useSetRecoilState(draggingTaskAtom);
  const scrollTo = useRecoilValue(scrollToAtom);

  const handleOnDragEnd = async (result) => {
    unhideAllRows();
    setDraggingTask(null);
    window.scrollTo({ top: scrollTo });
    // const firstRow = window.document.getElementById("nobody");
    // firstRow.style.height = "";
    // const lastRow = window.document.getElementById("bottom");
    // lastRow.style.height = "";
    const taskId = result.draggableId;
    const currentTask = taskQuery.data.find((task) => task.id === taskId);
    const destColumnId = result.destination.droppableId;

    const [destTeammateId, destDate] = destColumnId.split("|");

    //FIND ALL TASKS FROM THE SAME PROJECT THAT HAVE A SORT ORDER GREATER THAN CURRENT TASK
    let projectTaskWithDates = [];
    let projectTasksToUpdate = [];
    let restOfTasks = [];
    taskQuery.data.forEach((task) => {
      if (task.projectNo === currentTask.projectNo && task.date) {
        projectTaskWithDates.push(task);
      }
      if (
        task.projectNo === currentTask.projectNo &&
        task.sortOrder >= currentTask.sortOrder
      ) {
        projectTasksToUpdate.push(task);
      } else {
        restOfTasks.push(task);
      }
    });

    //DETERMINE IF ANY OF THE TASKS HAVE A SMALLER SORT NUMBER AND LARGER DATE
    const taskDate = dayjs(destDate, "M/D/YYYY");
    const taskWithLaterDateAndEarlierSort = projectTaskWithDates.find(
      (projTask) => {
        const projDate = dayjs(projTask.date, "M/D/YYYY");
        return (
          taskDate.isBefore(projDate) &&
          projTask.sortOrder < currentTask.sortOrder
        );
      }
    );

    //Revert if tasks where dragged into an incorrect order
    if (taskWithLaterDateAndEarlierSort) {
      alert(
        `Error: You can't drag this task before the ${taskWithLaterDateAndEarlierSort.department} task`
      );
      return;
    }

    //UPDATE PROJECT TASKS WITH PROPER DATE AND SORT ORDER
    // Only update the current tasks teammateID
    const projectTasks = updateProjectTasks(
      projectTasksToUpdate,
      destTeammateId,
      destDate,
      taskId
    );

    //Optimistically update tasks
    const taskSnapshot = [...taskQuery.data];
    queryClient.setQueryData(
      ["tasks", weekFilter],
      [...projectTasks, ...restOfTasks]
    );
    setForceUpdate(new Date());

    if (!testMode) {
      try {
        //script: task_UPDATE_formTaskBoard
        await fmScript(UPDATE_TASK_SCRIPT, { tasks: projectTasks });
      } catch (e) {
        console.log(e);
        //Revert to previous state and refetch
        queryClient.setQueryData(["tasks", weekFilter], taskSnapshot);
        taskQuery.refetch();
        alert(e.message);
      }
    }
  };

  return handleOnDragEnd;
};
