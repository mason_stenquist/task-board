import { useMemo } from "preact/hooks";
import { useRecoilValue } from "recoil";
import { departmentFilterAtom } from "../state";

export const useSortedTeammates = (filteredTeammates, forceUpdate) => {
  const departmentFilter = useRecoilValue(departmentFilterAtom);

  const sortedTeammates = useMemo(() => {
    if (!departmentFilter) return filteredTeammates;
    return filteredTeammates.sort((a, b) => {
      if (!a.departments || a.departments.length === 0) return -1;

      const aDepartment = (a.departments || []).find(
        (dep) => dep.departmentID === departmentFilter
      );

      const bDepartment = (b.departments || []).find(
        (dep) => dep.departmentID === departmentFilter
      );
      const aPriority = aDepartment ? aDepartment.priority : 0;
      const bPriority = bDepartment ? bDepartment.priority : 0;

      if (aPriority < bPriority) return -1;
      if (aPriority > bPriority) return 1;
      return a.nickName.toLowerCase() < b.nickName.toLowerCase();
    });
  }, [filteredTeammates, forceUpdate]);

  return sortedTeammates;
};
