import { useMemo } from "preact/hooks";
import useTasks from "../api/useTasks";
import dayjs from "dayjs";
import weekday from "dayjs/plugin/weekday";
import weekOfYear from "dayjs/plugin/weekOfYear";
import weekYear from "dayjs/plugin/weekYear";
import advancedFormat from "dayjs/plugin/advancedFormat";

dayjs.extend(weekday);
dayjs.extend(weekday);
dayjs.extend(weekOfYear);
dayjs.extend(weekYear);
dayjs.extend(advancedFormat);

const sortTasks = (taskA, taskB) => {
  if (taskA.prioritySort < taskB.prioritySort) return -1;
  if (taskA.prioritySort > taskB.prioritySort) return 1;
  if (taskA.sortOrder < taskB.sortOrder) return -1;
  if (taskA.sortOrder > taskB.sortOrder) return 1;
  return 0;
};

export const useRowFormatter = (sortedTeammates, forceUpdate) => {
  const taskQuery = useTasks();

  const rows = useMemo(() => {
    if (taskQuery.isLoading) return [];
    const monday = dayjs().weekday(0);
    const teammateMap = {};
    const dateMap = {};

    //Add date columns and task arrays to teammates data
    const teammates = sortedTeammates.map((person, i) => {
      teammateMap[person.id] = i;

      //Generate column for unassigned and each day of week
      const columns = [
        {
          id: `${person.id}|`,
          date: null,
          items: [],
          capacity: 0,
        },
      ];

      //Create column for each day of week
      for (let i = 1; i <= 5; i++) {
        const day = monday.add(i, "day");
        const formattedDate = day.format("M/D/YYYY");
        const dayOfWeek = day.format("dddd");
        columns.push({
          id: `${person.id}|${formattedDate}`,
          date: formattedDate,
          items: [],
          capacity: person.defaultCapacity[dayOfWeek.toLowerCase()] ?? 0,
        });
        dateMap[formattedDate] = i;
      }

      return {
        ...person,
        columns,
      };
    });

    //Link tasks to correct teammate and date
    const sortedTasks = taskQuery.data.sort(sortTasks);
    sortedTasks.forEach((task) => {
      const teammateIndex = teammateMap[task.teammateID] ?? 0;
      const dateIndex = !task.date ? 0 : dateMap[task.date];
      try {
        //Any dates above 5 we're showing don't need to be in our structure
        if (dateIndex >= 0) {
          teammates[teammateIndex].columns[dateIndex].items.push(task);
        }
      } catch (e) {
        console.log(e);
        console.log(task);
      }
    });

    return teammates;
  }, [sortedTeammates, taskQuery.data, forceUpdate]);

  return rows;
};
