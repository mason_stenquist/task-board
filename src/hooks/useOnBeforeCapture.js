import { useRecoilState, useSetRecoilState } from "recoil";
import useTasks from "../api/useTasks";
import { draggingTaskAtom, scrollToAtom } from "../state";

export const useOnBeforeCapture = (filteredTeammates) => {
  const taskQuery = useTasks();
  const [scrollTo, setScrollTo] = useRecoilState(scrollToAtom);
  const setDraggingTask = useSetRecoilState(draggingTaskAtom);

  const handleBeforeCapture = (results) => {
    setScrollTo(window.scrollY);
    const id = results.draggableId;
    const task = taskQuery.data.find((task) => task.id === id);
    setDraggingTask(task);

    //find teammates that can do this task
    const teammatesThatCannotDoTask = filteredTeammates.filter((teammate) => {
      if (teammate.id === "nobody") return false;
      if (teammate.id === task.teammateID) return false;
      return !(teammate.departments || []).some(
        (dep) => dep.departmentID === task.departmentID
      );
    });

    teammatesThatCannotDoTask.forEach((teammate) => {
      const row = window.document.getElementById(teammate.id);
      row.classList.add("hidden");
    });
  };

  return handleBeforeCapture;
};
