import { useMemo } from "preact/hooks";
import useHolidays from "../api/useHolidays";

const useGetHoliday = (date) => {
  const holidayQuery = useHolidays();

  const holiday = useMemo(() => {
    if (!holidayQuery.data) return;
    const found = holidayQuery.data.find((holiday) => holiday.date === date);
    return found;
  }, [date]);

  return holiday;
};
export default useGetHoliday;
