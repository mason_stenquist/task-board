import { useMemo } from "preact/hooks";
import useTeammates from "../api/useTeammates";
import { useRecoilValue } from "recoil";
import { departmentFilterAtom } from "../state";

export const useFilteredTeammates = (forceUpdate) => {
  const teammatesQuery = useTeammates();
  const departmentFilter = useRecoilValue(departmentFilterAtom);

  const filteredTeammates = useMemo(() => {
    if (teammatesQuery.isLoading) return [];
    if (!departmentFilter) return teammatesQuery.data;
    return teammatesQuery.data.filter((teammate) => {
      if (teammate.id === "nobody") return true;
      return (teammate.departments || []).some(
        (dep) => dep.departmentID === departmentFilter
      );
    });
  }, [teammatesQuery, departmentFilter, forceUpdate]);

  return filteredTeammates;
};
