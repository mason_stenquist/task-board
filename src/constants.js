export const PRIORITY = {
  Extreme: "Extreme",
  High: "High",
  Normal: "Normal",
  Low: "Low",
  Backup: "Backup",
};

export const PRIORITY_NUM = {
  [PRIORITY.Extreme]: 1,
  [PRIORITY.High]: 2,
  [PRIORITY.Normal]: 3,
  [PRIORITY.Low]: 4,
  [PRIORITY.Backup]: 5,
};

//task_UPDATE_formTaskBoard
export const UPDATE_TASK_SCRIPT = 873;

//task_OPEN_taskModal
export const OPEN_TASK_MODAL_SCRIPT = 884;

//task_UPDATE_priorityFromTaskBoard
export const UPDATE_PRIORITY_SCRIPT = 934;

//task_UPDATE_weekFromTaskBoard
export const UPDATE_WEEK_SCRIPT = 936;
