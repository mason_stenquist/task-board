export const roundNumber = (num, decimals = 2) => {
  const roundByString =
    "1" +
    "00000000000000000000000000000000000000000000000000000000000000000000000000".substr(
      0,
      decimals
    );
  const roundBy = parseFloat(roundByString);
  if (typeof num !== "number") return 0;
  return Math.round((num + Number.EPSILON) * roundBy) / roundBy;
};
