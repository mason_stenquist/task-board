import classNames from "classnames";
import { useMemo } from "preact/hooks";
import { roundNumber } from "../utils/roundNumber";

const ColumnOverviewIndicator = ({ totalEstimated, capacity }) => {
  const capacityPercent = useMemo(() => {
    return roundNumber((totalEstimated / capacity) * 100, 0);
  }, [totalEstimated]);

  const progressContainerClasses = classNames(
    "top-0 left-0 bg-gray-100 h-[10px] relative  overflow-hidden border-b mx-2 mt-2 rounded-full",
    {
      "ring ring-green-400": capacity > 0 && totalEstimated === 0,
    }
  );

  const progressBarClasses = classNames("h-full", {
    "bg-green-400": capacityPercent >= 70 && capacityPercent <= 100,
    "bg-yellow-400": capacityPercent < 70,
    "bg-red-400": capacityPercent > 100,
  });

  return (
    <div className={progressContainerClasses}>
      <div
        className={progressBarClasses}
        style={{ width: `${capacityPercent}%` || 0 }}
      ></div>
    </div>
  );
};

export default ColumnOverviewIndicator;
