import LocalFireDepartmentIcon from "../icons/LocalFireDepartmentIcon";
import TrendingUpIcon from "../icons/TrendingUpIcon";
import TrendingFlatIcon from "../icons/TrendingFlatIcon";
import TrendingDownIcon from "../icons/TrendingDownIcon";
import HistoryIcon from "../icons/HistoryIcon";
import { PRIORITY } from "../constants";
import { memo } from "preact/compat";

const CardPriorityIcon = ({ priority }) => {
  const size = "w-5 h-5 mr-1";
  if (priority === PRIORITY.Extreme)
    return (
      <LocalFireDepartmentIcon
        className={`text-red-600  ${size}`}
        title={`${PRIORITY.Extreme} Priority`}
      />
    );

  if (priority === PRIORITY.High)
    return (
      <TrendingUpIcon
        className={`text-yellow-600  ${size}`}
        title={`${PRIORITY.High} Priority`}
      />
    );

  if (priority === PRIORITY.Normal)
    return (
      <TrendingFlatIcon
        className={`text-gray-600  ${size}`}
        title={`${PRIORITY.Normal} Priority`}
      />
    );

  if (priority === PRIORITY.Low)
    return (
      <TrendingDownIcon
        className={`text-blue-600  ${size}`}
        title={`${PRIORITY.Low} Priority`}
      />
    );

  if (priority === PRIORITY.Backup)
    return (
      <HistoryIcon
        className={`text-green-600  ${size}`}
        title={`${PRIORITY.Backup} Priority`}
      />
    );

  return null;
};

export default memo(CardPriorityIcon);
