import { useMemo } from "preact/hooks";
import useTasks from "../api/useTasks";
import useTeammates from "../api/useTeammates";
import useGetHoliday from "../hooks/useGetHoliday";
import { roundNumber } from "../utils/roundNumber";
import ColumnOverviewIndicator from "./ColumnOverviewIndicator";

const DayOverview = ({ total, day }) => (
  <div className="px-2 pb-1 flex  items-center justify-between">
    <p className="text-sm font-bold">
      {day.dayOfWeek} {day.dayNumber}
    </p>
    <p className="text-[10px] text-gray-600">
      <strong className="text-black">{total.tasks}</strong> Tasks,{" "}
      <strong className="text-black">{total.hoursEst}</strong> Est Hrs,{" "}
      <strong className="text-black">{total.capacity}</strong> Capacity
    </p>
  </div>
);

const HeaderDay = ({ day }) => {
  const taskQuery = useTasks();
  const teammatesQuery = useTeammates();

  const holiday = useGetHoliday(day.formattedDate);

  const total = useMemo(() => {
    if (taskQuery.isLoading || teammatesQuery.isLoading)
      return { tasks: "?", hoursEst: "?", capacity: "?" };

    const filteredTasks = taskQuery.data.filter(
      (task) => task.date == day.formattedDate
    );

    const hoursEst = filteredTasks.reduce(
      (total, task) => total + task.hoursEst,
      0
    );

    const capacity =
      day.dayOfWeek == "Unassigned"
        ? null
        : teammatesQuery.data.reduce(
            (total, teammate) =>
              total +
              Number(
                teammate.defaultCapacity[day.dayOfWeekFullName.toLowerCase()] ??
                  0
              ),
            0
          );

    return {
      tasks: filteredTasks.length,
      hoursEst: roundNumber(hoursEst, 0),
      capacity: roundNumber(capacity, 0),
    };
  }, [taskQuery, teammatesQuery, day.formattedDate]);

  return (
    <div className="flex-1 border-b  relative">
      <ColumnOverviewIndicator
        totalEstimated={total.hoursEst}
        capacity={total.capacity}
      />
      {holiday ? (
        <div className="px-2 pb-1 text-sm">
          {holiday.emoji} <strong>{holiday.name}</strong>
        </div>
      ) : (
        <DayOverview total={total} day={day} />
      )}
    </div>
  );
};
export default HeaderDay;
