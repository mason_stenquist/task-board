import { useEffect, useState } from "preact/hooks";
import React from "react";
import { DragDropContext } from "react-beautiful-dnd";
import useTasks from "../api/useTasks";
import Row from "./Row";
import Header from "./Header";
import { useFilteredTeammates } from "../hooks/useFilteredTeammates";
import { useSortedTeammates } from "../hooks/useSortedTeammates";
import { useRowFormatter } from "../hooks/useRowFormatter";
import { useOnDragEnd } from "../hooks/useOnDragEnd";
import { useOnBeforeCapture } from "../hooks/useOnBeforeCapture";

function Board() {
  const [forceUpdate, setForceUpdate] = useState(new Date());
  const taskQuery = useTasks();
  const filteredTeammates = useFilteredTeammates(forceUpdate);
  const sortedTeammates = useSortedTeammates(filteredTeammates, forceUpdate);
  const rows = useRowFormatter(sortedTeammates, forceUpdate);
  const handleOnDragEnd = useOnDragEnd(rows, setForceUpdate);
  const handleBeforeCapture = useOnBeforeCapture(filteredTeammates);

  //Setup hook on window to reload data
  useEffect(() => {
    window.refetchTasks = () => {
      taskQuery.refetch();
    };
  }, []);

  if (taskQuery.isLoading)
    return (
      <div className="flex h-screen justify-center items-center">
        Loading...
      </div>
    );

  return (
    <div className="border mb-20">
      <Header />
      <DragDropContext
        onDragEnd={handleOnDragEnd}
        onBeforeCapture={handleBeforeCapture}
      >
        {rows.map((row) => (
          <Row key={row.id} row={row} />
        ))}
      </DragDropContext>
    </div>
  );
}

export default Board;
