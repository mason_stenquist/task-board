import classNames from "classnames";
import { Droppable } from "react-beautiful-dnd";
import Card from "./Card";
import ColumnOverview from "./ColumnOverview";

const Column = ({ columnId, column, collapsed, index }) => {
  const estimatedHoursClasses = classNames("text-sm font-bold", {
    "text-red-500": column.estimatedHours > 40,
  });
  return (
    <div className="select-none flex-1" key={columnId}>
      <div className="bg-white h-full">
        <div className="h-full">
          <Droppable droppableId={columnId} key={columnId}>
            {(provided, snapshot) => {
              const columnClasses = classNames(
                "  h-full transition-all duration-200 relative",
                {
                  "bg-blue-100": snapshot.isDraggingOver,
                },
                {
                  "min-h-[100px] space-y-1 py-1 pt-[60px]": !collapsed,
                },
                {
                  "min-h-[60px] max-h-[60px] overflow-hidden": collapsed,
                }
              );

              return (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  className={columnClasses}
                >
                  <ColumnOverview column={column} />
                  {!collapsed &&
                    column.items.map((item, index) => {
                      return <Card key={item.id} item={item} index={index} />;
                    })}
                  {provided.placeholder}
                </div>
              );
            }}
          </Droppable>
        </div>
      </div>
    </div>
  );
};

export default Column;
