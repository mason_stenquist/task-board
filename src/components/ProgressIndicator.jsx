import classNames from "classnames";
import { roundNumber } from "../utils/roundNumber";

const ProgressIndicator = ({ item }) => {
  const est = roundNumber(item.hoursEst);
  const act = roundNumber(item.hoursAct);
  const percentComplete = est === 0 ? 0 : roundNumber((act / est) * 100, 0);

  const indicatorClasses = classNames("h-1 rounded", {
    "bg-blue-500": percentComplete <= 100,
    "bg-red-600": percentComplete > 100,
  });

  return (
    <div>
      <div className="w-[80px] overflow-hidden bg-gray-200 shadow-inner rounded">
        <div
          className={indicatorClasses}
          style={{ width: `${percentComplete}%` }}
        ></div>
      </div>
      <div className="text-[10px] text-right">
        {act} / {est} ({percentComplete}%)
      </div>
    </div>
  );
};

export default ProgressIndicator;
