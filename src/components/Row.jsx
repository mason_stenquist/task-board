import classNames from "classnames";
import { useState } from "preact/hooks";
import React, { useMemo } from "react";
import { useRecoilValue } from "recoil";
import KeyboardArrowDownIcon from "../icons/KeyboardArrowDownIcon";
import { departmentFilterAtom } from "../state";
import Column from "./Column";

const Row = ({ row }) => {
  const [collapsed, setCollapsed] = useState(row.collapsed);
  const departmentFilter = useRecoilValue(departmentFilterAtom);

  const departmentDisplay = useMemo(() => {
    if (!departmentFilter || !row.departments) return "";
    const currentDepartment = row.departments.find(
      (dep) => dep.departmentID === departmentFilter
    );
    return currentDepartment
      ? `Priority: ${currentDepartment.priority}\nSkill: ${currentDepartment.skill}`
      : "";
  }, [departmentFilter, row]);

  const rowClasses = classNames("row divide-x divide-y flex ");

  const arrowIconClasses = classNames(
    "text-black w-6 h-6  transition-all duration-200 transform",
    {
      "rotate-180": !collapsed,
    }
  );

  return (
    <div id={row.id} class={rowClasses}>
      <button
        onClick={() => setCollapsed((prev) => !prev)}
        className="border-t min-w-[120px] max-w-[120px] px-2 pt-2 text-xs text-left flex flex-col"
      >
        <p className="flex justify-between font-bold w-full items-start">
          {row.nickName} <KeyboardArrowDownIcon className={arrowIconClasses} />
        </p>
        <p className="-mt-1 whitespace-pre-wrap text-[10px]">
          {departmentDisplay}
        </p>
      </button>
      {(row.columns || []).map((column, index) => {
        return (
          <Column
            key={column.id}
            columnId={column.id}
            column={column}
            index={index}
            collapsed={collapsed}
          />
        );
      })}
    </div>
  );
};

export default Row;
