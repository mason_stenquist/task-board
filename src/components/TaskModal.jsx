import CardPriorityIcon from "./CardPriorityIcon";
import ProgressIndicator from "./ProgressIndicator";

const TaskModal = ({ task, customerAndCraft }) => {
  return (
    <div className="">
      <div className="flex justify-between items-center">
        <p className="font-bold text-xs flex items-center">
          <CardPriorityIcon priority={task?.priority} />
          {task.department} - {task.projectNo}
        </p>
        <ProgressIndicator item={task} />
      </div>
      <p class="text-xs font-bold" title={customerAndCraft}>
        {customerAndCraft}
      </p>
      <p class="text-[10px] leading-3 pt-0.5 text-gray-700 mb-4">
        {task.projectInfo}
      </p>
      <div></div>
      <div className="flex space-x-4">
        <div>List Of Tasks</div>
        <div>Quick Actions</div>
      </div>
    </div>
  );
};

export default TaskModal;
