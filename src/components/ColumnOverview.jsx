import { useMemo } from "preact/hooks";
import { roundNumber } from "../utils/roundNumber";
import ColumnOverviewIndicator from "./ColumnOverviewIndicator";

const ColumnOverview = ({ column }) => {
  const totalEstimated = useMemo(() => {
    return roundNumber(
      column.items.reduce((total, item) => total + item.hoursEst, 0),
      2
    );
  }, [column]);

  return (
    <div className="absolute top-0 w-full h-[55px]">
      <ColumnOverviewIndicator
        totalEstimated={totalEstimated}
        capacity={column.capacity}
      />
      <div className="w-full absolute px-1 pt-2 text-xs justify-evenly text-gray-400 flex">
        <p>
          <strong>{column.items.length}</strong> Tasks
        </p>
        <p>
          <strong>{totalEstimated}</strong> Est. Hrs
        </p>
        <p>
          <strong>{column.capacity}</strong> Capacity
        </p>
      </div>
    </div>
  );
};

export default ColumnOverview;
