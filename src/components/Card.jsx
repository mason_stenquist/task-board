import classNames from "classnames";
import { useMemo } from "react";
import { Draggable } from "react-beautiful-dnd";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { fmScript } from "fmcperformscript";
import {
  contextMenuAtom,
  departmentFilterAtom,
  draggingTaskAtom,
  taskStatusFilterAtom,
} from "../state";
import CardContextMenu from "./CardContextMenu";
import CardPriorityIcon from "./CardPriorityIcon";
import ProgressIndicator from "./ProgressIndicator";
import { OPEN_TASK_MODAL_SCRIPT } from "../constants";
import useTasks from "../api/useTasks";

const Card = ({ item, index }) => {
  const setContextMenu = useSetRecoilState(contextMenuAtom);
  const departmentFilter = useRecoilValue(departmentFilterAtom);
  const taskStatusFilter = useRecoilValue(taskStatusFilterAtom);
  const draggingTask = useRecoilValue(draggingTaskAtom);

  const handleContextMenu = (e) => {
    e.preventDefault();
    setContextMenu(<CardContextMenu e={e} task={item} />);
  };

  const handleCardClick = (e) => {
    fmScript(OPEN_TASK_MODAL_SCRIPT, {
      projectId: item.projectID,
      taskId: item.id,
    });
  };

  const customerAndCraft = useMemo(() => {
    const craftName = item.craftName ? ` - ${item.craftName}` : "";
    return item.customer + craftName;
  }, [item]);

  //Filter tasks
  if (departmentFilter && departmentFilter !== item.departmentID) return null;
  if (taskStatusFilter && !taskStatusFilter.includes(item.status)) return null;

  return (
    <Draggable key={item.id} draggableId={item.id} index={index}>
      {(provided, snapshot) => {
        const cardClasses = classNames(
          "border-l-4 card select-none p-1 min-h-[50px] bg-white rounded-sm border text-sm transition-all duration-400",
          {
            "bg-blue-50 shadow-lg opacity-50": snapshot.isDragging,
          },
          {
            "shadow-sm": !snapshot.isDragging,
          },
          {
            "border-green-500": item.status == "Current",
          },
          {
            "border-yellow-400": item.status == "Pending",
          },
          {
            "border-red-400": item.status == "Hold",
          },
          {
            "ring-4  bg-yellow-50": item.projectNo === draggingTask?.projectNo,
          }
        );

        return (
          <div
            className="px-1"
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            style={{
              ...provided.draggableProps.style,
            }}
          >
            <div
              className={cardClasses}
              onContextMenu={handleContextMenu}
              onClick={handleCardClick}
            >
              <div className="flex justify-between items-center">
                <p className="font-bold text-xs flex items-center">
                  <CardPriorityIcon priority={item?.priority} />
                  {item.department} - {item.projectNo}
                </p>
                <ProgressIndicator item={item} />
              </div>
              <p
                class="text-xs h-4 font-bold overflow-hidden"
                title={customerAndCraft}
              >
                {customerAndCraft}
              </p>
              <p class="text-[10px] leading-3 pt-0.5 text-gray-700">
                {item.projectInfo}
              </p>
            </div>
          </div>
        );
      }}
    </Draggable>
  );
};

export default Card;
