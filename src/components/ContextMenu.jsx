import { useEffect, useRef } from "preact/hooks";
import { useSetRecoilState } from "recoil";
import { contextMenuAtom } from "../state";

const ContextMenu = ({ mouseEvent, children }) => {
  const menuRef = useRef();
  const setContextMenu = useSetRecoilState(contextMenuAtom);
  const { clientX, clientY } = mouseEvent;

  const closeMenu = (e) => {
    setContextMenu(null);
  };

  useEffect(() => {
    window.document.addEventListener("click", closeMenu);
    return () => {
      window.document.removeEventListener("click", closeMenu);
    };
  }, []);
  return (
    <div
      ref={menuRef}
      className="bg-white border shadow-lg fixed flex flex-col rounded overflow-hidden z-10"
      style={{ top: clientY - 4, left: clientX - 2 }}
    >
      {children}
    </div>
  );
};

ContextMenu.Separator = ({ children, ...rest }) => {
  return (
    <div className="text-xs text-gray-500 px-2.5 py-2 border-t" {...rest}>
      {children}
    </div>
  );
};

ContextMenu.Item = ({ Icon, children, ...rest }) => {
  return (
    <button
      className="text-sm text-left py-2 px-2.5 hover:bg-gray-50 flex space-x-2 items-center"
      {...rest}
    >
      {Icon && <Icon className="w-5 h-5 text-black" />}
      <span>{children}</span>
    </button>
  );
};

export default ContextMenu;
