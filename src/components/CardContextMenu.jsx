import { useQueryClient } from "react-query";
import { useRecoilValue } from "recoil";
import {
  PRIORITY,
  PRIORITY_NUM,
  UPDATE_PRIORITY_SCRIPT,
  UPDATE_WEEK_SCRIPT,
} from "../constants";
import HistoryIcon from "../icons/HistoryIcon";
import LocalFireDepartmentIcon from "../icons/LocalFireDepartmentIcon";
import TrendingDownIcon from "../icons/TrendingDownIcon";
import TrendingFlatIcon from "../icons/TrendingFlatIcon";
import TrendingUpIcon from "../icons/TrendingUpIcon";
import { testModeAtom, weekFilterAtom } from "../state";
import { fmScript } from "fmcperformscript";

import ContextMenu from "./ContextMenu";

const CardContextMenu = ({ e, task }) => {
  const testMode = useRecoilValue(testModeAtom);
  const weekFilter = useRecoilValue(weekFilterAtom);
  const queryClient = useQueryClient();

  const moveToNextWeek = async () => {
    if (!testMode) {
      try {
        await fmScript(UPDATE_WEEK_SCRIPT, { id: task.id, date: task.date });
      } catch (e) {
        console.log(e);
        return;
      }
    }
    queryClient.setQueryData(["tasks", weekFilter], (old) => {
      const withoutCurrentTask = old.filter((t) => t.id !== task.id);
      return withoutCurrentTask;
    });
  };

  const changePriority = async (priority) => {
    console.log(PRIORITY_NUM[priority]);

    if (!testMode) {
      try {
        await fmScript(UPDATE_PRIORITY_SCRIPT, { id: task.id, priority });
      } catch (e) {
        console.log(e);
        return;
      }
    }

    queryClient.setQueryData(["tasks", weekFilter], (old) => {
      const withoutCurrentTask = old.filter((t) => t.id !== task.id);
      const updatedTask = {
        ...task,
        priority,
        prioritySort: PRIORITY_NUM[priority],
      };
      console.log(updatedTask);
      return [...withoutCurrentTask, updatedTask];
    });
  };

  return (
    <ContextMenu mouseEvent={e}>
      <ContextMenu.Item onClick={(e) => moveToNextWeek()}>
        Move To Next Week
      </ContextMenu.Item>
      <ContextMenu.Item>Go To Task</ContextMenu.Item>
      <ContextMenu.Separator>Change Status To</ContextMenu.Separator>
      <ContextMenu.Item
        onClick={(e) => changePriority(PRIORITY.Extreme)}
        Icon={LocalFireDepartmentIcon}
      >
        Extreme
      </ContextMenu.Item>
      <ContextMenu.Item
        onClick={(e) => changePriority(PRIORITY.High)}
        Icon={TrendingUpIcon}
      >
        High
      </ContextMenu.Item>
      <ContextMenu.Item
        onClick={(e) => changePriority(PRIORITY.Normal)}
        Icon={TrendingFlatIcon}
      >
        Normal
      </ContextMenu.Item>
      <ContextMenu.Item
        onClick={(e) => changePriority(PRIORITY.Low)}
        Icon={TrendingDownIcon}
      >
        Low
      </ContextMenu.Item>
      <ContextMenu.Item
        onClick={(e) => changePriority(PRIORITY.Backup)}
        Icon={HistoryIcon}
      >
        Backup
      </ContextMenu.Item>
    </ContextMenu>
  );
};

export default CardContextMenu;
