import { useRecoilState } from "recoil";

import { taskStatusFilterAtom, testModeAtom } from "../../state";
import { WeekFilter } from "./WeekFilter";
import { DepartmentFilter } from "./DepartmentFilter";
import { StatusFilter } from "./StatusFilter";

export function Filters() {
  const [testMode, setTestMode] = useRecoilState(testModeAtom);

  return (
    <div className="flex items-center justify-between bg-gray-700 p-2">
      <div className="flex space-x-2  ">
        <WeekFilter />
        <DepartmentFilter />
        <StatusFilter />
      </div>
      <div>
        <label
          title="While in test mode, nothing will be saved to the database and all changes will be lost"
          className="flex space-x-2 text-white items-center "
        >
          <span>Test Mode</span>
          <input
            type="checkbox"
            value="true"
            checked={testMode}
            onChange={(e) => setTestMode(e.target.checked)}
          />
        </label>
      </div>
    </div>
  );
}
