import { useEffect, useMemo } from "preact/hooks";
import { useRecoilState } from "recoil";
import { weekFilterAtom, weekStartDateAtom } from "../../state";
import dayjs from "dayjs";
import advancedFormat from "dayjs/plugin/advancedFormat";
import weekOfYear from "dayjs/plugin/weekOfYear";

dayjs.extend(advancedFormat);
dayjs.extend(weekOfYear);

export const WEEKS_BEFORE = 8;

export function WeekFilter() {
  const [weekFilter, setWeekFilter] = useRecoilState(weekFilterAtom);
  const [weekStartDate, setWeekStartDate] = useRecoilState(weekStartDateAtom);

  const weeks = useMemo(() => {
    const array = [];
    const start = dayjs().weekday(0);
    for (let i = -WEEKS_BEFORE; i < 8; i++) {
      const date = start.add(i, "week");
      const day = date.toISOString();
      const week = date.format("gggg.w");
      array.push({ day, week });
    }
    return array;
  }, []);

  //Make sure that the selected week is one of the dates in our array
  useEffect(() => {
    setWeekStartDate(weeks[WEEKS_BEFORE].day);
    setWeekFilter(weeks[WEEKS_BEFORE].week);
  }, []);

  const handleOnChange = (e) => {
    const start = dayjs(e.target.value);
    const week = start.format("gggg.w");
    setWeekFilter(week);
    setWeekStartDate(e.target.value);
  };

  return (
    <select
      placeholder="Week"
      className="border rounded p-2"
      value={weekStartDate}
      onChange={handleOnChange}
    >
      {weeks.map((week) => (
        <option value={week.day}>{week.week}</option>
      ))}
    </select>
  );
}
