import { useRecoilState } from "recoil";
import { departmentFilterAtom } from "../../state";
import useDepartments from "../../api/useDepartments";

export function DepartmentFilter() {
  const [departmentFilter, setDepartmentFilter] =
    useRecoilState(departmentFilterAtom);

  const departmentQuery = useDepartments();

  return (
    <select
      placeholder="Department"
      className="border rounded p-2"
      value={departmentFilter}
      onChange={(e) => setDepartmentFilter(e.target.value)}
    >
      <option value="">All Departments</option>
      {(departmentQuery.data || []).map((department) => (
        <option value={department.id}>{department.name}</option>
      ))}
    </select>
  );
}
