import { useRecoilState } from "recoil";
import { taskStatusFilterAtom } from "../../state";

export function StatusFilter() {
  const [taskStatusFilter, setTaskStatusFilter] =
    useRecoilState(taskStatusFilterAtom);

  return (
    <select
      placeholder="Week"
      className="border rounded p-2"
      value={taskStatusFilter}
      onChange={(e) => setTaskStatusFilter(e.target.value)}
    >
      <option value={["Current", "Pending"]}>Incomplete Tasks</option>
      <option value={["Current"]}>Current Tasks</option>
      <option value={["Pending"]}>Pending Tasks</option>
      <option value={["Complete"]}>Complete Tasks</option>
      <option value="">All Tasks</option>
    </select>
  );
}
