import { useMemo } from "preact/hooks";
import dayjs from "dayjs";
import weekday from "dayjs/plugin/weekday";
import weekOfYear from "dayjs/plugin/weekOfYear";
import weekYear from "dayjs/plugin/weekYear";
import advancedFormat from "dayjs/plugin/advancedFormat";
import customParseFormat from "dayjs/plugin/customParseFormat";
import HeaderDay from "./HeaderDay";
import { useRecoilValue } from "recoil";
import { weekStartDateAtom } from "../state";

dayjs.extend(weekday);
dayjs.extend(weekOfYear);
dayjs.extend(weekYear);
dayjs.extend(advancedFormat);
dayjs.extend(customParseFormat);

const Header = () => {
  const weekStartDate = useRecoilValue(weekStartDateAtom);

  const days = useMemo(() => {
    const daysArray = [
      { date: null, formattedDate: "", dayOfWeek: "Unassigned", dayNumber: "" },
    ];
    const monday = dayjs(weekStartDate);
    for (let i = 1; i <= 5; i++) {
      const date = monday.add(i, "day");
      const formattedDate = date.format("M/D/YYYY");
      const dayOfWeek = date.format("ddd");
      const dayOfWeekFullName = date.format("dddd");
      const dayNumber = date.format("D");
      daysArray.push({
        date,
        formattedDate,
        dayOfWeek,
        dayOfWeekFullName,
        dayNumber,
      });
    }
    return daysArray;
  }, [weekStartDate]);

  return (
    <div className="flex divide-x divide-gray-300 sticky top-0 z-10 bg-gray-200 shadow-md">
      <div className="min-w-[120px] p-1  border-b flex items-center justify-between">
        <p className="text-sm font-bold">Person</p>
      </div>
      {days.map((day) => (
        <HeaderDay key={day.dayOfWeek} day={day} />
      ))}
    </div>
  );
};
export default Header;
