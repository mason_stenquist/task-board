const KeyboardArrowRightIcon = ({
  className = "text-black w-6 h-6",
  title = null,
}) => {
  return (
    <div title={title}>
      <svg
        className={`${className} fill-current`}
        focusable="false"
        viewBox="0 0 24 24"
        aria-hidden="true"
        data-testid="KeyboardArrowRightIcon"
      >
        <path d="M8.59 16.59 13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z"></path>
      </svg>
    </div>
  );
};

export default KeyboardArrowRightIcon;
