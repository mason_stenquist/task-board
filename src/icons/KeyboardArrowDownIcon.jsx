const KeyboardArrowDownIcon = ({
  className = "text-black w-6 h-6",
  title = null,
}) => {
  return (
    <div title={title}>
      <svg
        className={`${className} fill-current`}
        focusable="false"
        viewBox="0 0 24 24"
        aria-hidden="true"
        data-testid="KeyboardArrowDownIcon"
      >
        <path d="M7.41 8.59 12 13.17l4.59-4.58L18 10l-6 6-6-6 1.41-1.41z"></path>
      </svg>
    </div>
  );
};

export default KeyboardArrowDownIcon;
