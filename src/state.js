import { atom } from "recoil";
import dayjs from "dayjs";
import weekday from "dayjs/plugin/weekday";
import weekOfYear from "dayjs/plugin/weekOfYear";
import weekYear from "dayjs/plugin/weekYear";
import advancedFormat from "dayjs/plugin/advancedFormat";
dayjs.extend(weekday);
dayjs.extend(weekOfYear);
dayjs.extend(weekYear);
dayjs.extend(advancedFormat);

export const contextMenuAtom = atom({
  key: "contextMenu",
  default: null,
});

export const weekStartDateAtom = atom({
  key: "weekStartDate",
  default: dayjs().weekday(0).toISOString(),
});

export const weekFilterAtom = atom({
  key: "weekFilter",
  default: dayjs().format("gggg.w"),
});

export const taskStatusFilterAtom = atom({
  key: "taskStatusFilter",
  default: ["Current", "Pending"],
});

export const departmentFilterAtom = atom({
  key: "departmentFilter",
  default: "",
});

export const testModeAtom = atom({
  key: "testMode",
  default: false,
});

export const draggingTaskAtom = atom({
  key: "draggingTask",
  default: null,
});

export const modalAtom = atom({
  key: "modal",
  default: undefined,
});

export const scrollToAtom = atom({
  key: "scrollTo",
  default: 0,
});
