import { useRecoilState, useRecoilValue } from "recoil";
import { contextMenuAtom, modalAtom } from "../state";
import Alert, { alertTextAtom } from "./components/Alert";
import ModalWrapper from "./components/ModalWrapper";

const Template = (props) => {
  const ContextMenu = useRecoilValue(contextMenuAtom);
  const modal = useRecoilValue(modalAtom);
  const [alertText, setAlertText] = useRecoilState(alertTextAtom);
  window.alert = (message) => setAlertText(message);
  return (
    <div>
      {alertText && <Alert />}
      {modal && <ModalWrapper />}
      {ContextMenu && ContextMenu}

      <div>{props.children}</div>
    </div>
  );
};
export default Template;
