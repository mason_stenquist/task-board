import { useRecoilState } from "recoil";
import { modalAtom } from "../../state";

const ModalWrapper = () => {
  const [modal, setModal] = useRecoilState(modalAtom);

  return (
    <div
      onClick={() => setModal(undefined)}
      class="fixed flex w-full h-full justify-center items-center z-20 bg-black bg-opacity-50"
    >
      <div>
        <div
          className="bg-white rounded p-4 relative"
          onClick={(e) => e.stopPropagation()}
        >
          <button
            className="absolute right-0 -mt-10"
            onClick={() => setModal(undefined)}
          >
            x
          </button>

          {modal}
        </div>
      </div>
    </div>
  );
};

export default ModalWrapper;
