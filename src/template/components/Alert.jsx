import { atom, useRecoilState } from "recoil";

export const alertTextAtom = atom({
  key: "alertText",
  default: null,
});

const Alert = () => {
  const [alertText, setAlertText] = useRecoilState(alertTextAtom);

  return (
    <div class="fixed flex w-full h-full justify-center items-center z-10">
      <div class="w-96 bg-white border shadow-lg p-4 rounded">
        <p class="py-2">{alertText}</p>
        <button
          onClick={() => setAlertText(null)}
          class="bg-gray-100 rounded border shadow px-4 py-1.5 hover:bg-gray-50"
        >
          Ok
        </button>
      </div>
    </div>
  );
};

export default Alert;
